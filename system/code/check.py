import os, time, subprocess, datetime

directories = {}
site_dir = '/root/Dropbox/molson/'

def regenerate():
    print("Changed, regenerating @",
          datetime.datetime.now() - datetime.timedelta(hours=5),
          "CST")
    subprocess.run(['python3', site_dir + 'system/code/generate.py'])
    subprocess.run(['cp', '-r', site_dir + 'system/site', '/'])
    subprocess.run(['cp', '-r', site_dir + 'images', '/site/'])
    subprocess.run(['chmod', '-R', '777', '/site'])


while True:
    changed = False
    for directory, _, _ in os.walk(site_dir):
        last = directories.get(directory)
        mtime = os.path.getmtime(directory)
        if not last or mtime > last:
            changed = True
            directories[directory] = mtime
    if changed:
        regenerate()
    time.sleep(1)
