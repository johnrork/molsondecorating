import os
import datetime
from collections import defaultdict
import CommonMark
from jinja2 import Environment, FileSystemLoader, select_autoescape

sysdir = os.path.join(os.path.dirname(__file__), os.pardir)
content_path = os.path.join(sysdir, os.pardir, 'content.txt')
image_dir = os.path.join(sysdir, os.pardir, 'images')
template_dir = os.path.join(sysdir, 'templates')
content_template = os.path.join(template_dir, 'rendered-content.html')
index_file = os.path.join(sysdir, 'site', 'index.html')

env = Environment(loader=FileSystemLoader(template_dir),
                  autoescape=select_autoescape(['html']))

with open(content_path, encoding='utf8') as content_file:
    content = CommonMark.commonmark(content_file.read())

with open(content_template, 'w+', encoding='utf8') as output_file:
    output_file.write(content)

ignore = ['.DS_Store']
images = defaultdict(list)

for file_tuple in os.walk(image_dir):
    directory, _, files = file_tuple
    for file in files:
        folder = os.path.basename(directory)
        print(directory,
              file,
              type(file),
              os.path.getmtime(
                  os.path.join(directory,file)))
        if file not in ignore:
            images[folder].append(file)

template = env.get_template('site.html')
print('images', images)
with open(index_file, 'w+', encoding='utf8') as f:
    f.write(template.render(now=datetime.datetime.now(), images=images))
