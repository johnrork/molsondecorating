echo "

Updating system"
apt-get update -y
apt-get install -y python3 python3-pip nautilus-dropbox nginx supervisor

echo "

Setting up python utils
"
pip3 install --upgrade pip
pip3 install --upgrade -r molsondecorating/system/setup/requirements.txt
echo "

Creating directories
"
mkdir /logs
mkdir /site
chmod -R 777 /site

echo "

Copying files
"
cd molsondecorating/system/setup
cp supervisord.conf /etc/supervisor/supervisord.conf
cp nginx.default /etc/nginx/sites-enabled/default

echo "

Starting services
"
nginx -s reload
dropbox start -i
dropbox stop
systemctl restart supervisor
